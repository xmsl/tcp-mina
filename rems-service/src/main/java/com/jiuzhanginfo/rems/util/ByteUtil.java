package com.jiuzhanginfo.rems.util;


/**
 * 
 * @author xumengsi
 *
 */
public class ByteUtil {
	
	  /**  
	    * byte数组中取int数值，本方法适用于(低位在前，高位在后)的顺序，和和intToBytes（）配套使用 
	    *   
	    * @param src  
	    *            byte数组  
	    * @param offset  
	    *            从数组的第offset位开始  
	    * @return int数值  
	    */    
	public static int bytesToInt(byte[] src, int offset) {  
	    int value;    
	    value = (int) ((src[offset] & 0xFF)   
	            | ((src[offset+1] & 0xFF)<<8)   
	            | ((src[offset+2] & 0xFF)<<16));  
	    return value;  
	}; 
	  
	 /**  
	    * byte数组中取int数值，本方法适用于(低位在后，高位在前)的顺序。和intToBytes2（）配套使用 
	    */  
	public static int bytesToInt2(byte[] src, int offset) {  
	    int value;    
	    value = (int) ( ((src[offset] & 0xFF)<<24)  
	            |((src[offset+1] & 0xFF)<<16)  
	            |((src[offset+2] & 0xFF)<<8)  
	            |(src[offset+3] & 0xFF));  
	    return value;  
	};  
	
	
	/**
     * byte[] 转为16进制String
     */
    public static String Bytes2HexString(byte[] b) { 
        String ret = ""; 
        for (int i = 0; i < b.length; i++) { 
            String hex = Integer.toHexString(b[i] & 0xFF); 
            if (hex.length() == 1) { 
                hex = '0' + hex; 
            } 
            ret += hex.toUpperCase(); 
        } 
        return ret; 
    }; 
    
    /**
     * 从一个byte[]数组中截取一部分
     * @param src
     * @param begin
     * @param count
     * @return
     */
    public static byte[] subBytes(byte[] src, int begin, int count) {
        byte[] bs = new byte[count];
        for (int i=begin;i<begin+count; i++) bs[i-begin] = src[i];
        return bs;
    };
    
    //     转化十六进制编码为字符串
    public static String toStringHex(String s)
    {
        byte[] baKeyword = new byte[s.length()/2];
        for(int i = 0; i < baKeyword.length; i++)
        {
          try
          {
              baKeyword[i] = (byte)(0xff & Integer.parseInt(s.substring(i*2, i*2+2),16));
          }
          catch(Exception e)
          {
              e.printStackTrace();
          }
        }
     
        try 
        {
            s = new String(baKeyword, "utf-8");//UTF-16le:Not
        } 
        catch (Exception e1) 
        {
            e1.printStackTrace();
        } 
        return s;
    };
    
    
    /**  
     * byte[]转int  
     * @param bytes  
     * @return  
     */  
    public static int byteArrayToInt(byte[] bytes) {  
           int value=0;  
           //由高位到低位  
           for(int i = 0; i < 4; i++) {  
               int shift= (4-1-i) * 8;  
               value +=(bytes[i] & 0x000000FF) << shift;//往高位游  
           }  
           return value;  
     }; 
}
