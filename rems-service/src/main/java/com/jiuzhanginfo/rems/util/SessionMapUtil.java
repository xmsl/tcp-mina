package com.jiuzhanginfo.rems.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.mina.core.session.IoSession;
/**
 * 
 * @author xumengsi
 *
 */
public class SessionMapUtil {
	
    private final static Log log = LogFactory.getLog(SessionMapUtil.class);
	
	private static SessionMapUtil sessionMap = null;
			
	private Map<String, IoSession>map = new HashMap<String, IoSession>();
	
	//构造私有化 单例
	private SessionMapUtil(){}
		
		
		/**
		 * 获取唯一实例
		 * @return
		 */
		public static SessionMapUtil newInstance(){
			if(sessionMap == null){
				sessionMap = new SessionMapUtil();
			}
			return sessionMap;
		};
		
		/**
		 * 保存session会话
		 * @param key
		 * @param session
		 */
		public void addSession(String key, IoSession session){
			log.debug("key=" + key);
			this.map.put(key, session);
		};
		
		/**
		 * 根据key查找缓存的session
		 * @param key
		 * @return
		 */
		public IoSession getSession(String key){
			log.debug("key=" + key);
			return this.map.get(key);
		};
		
		/**
		 * 发送消息到客户端
		 * @param keys
		 * @param message
		 */
		public void sendMessage(String[] keys, Object message){
			for(String key : keys){
				IoSession session = getSession(key);
				if(session == null){
					return;
				}
				session.write(message);
			}
		};

}
