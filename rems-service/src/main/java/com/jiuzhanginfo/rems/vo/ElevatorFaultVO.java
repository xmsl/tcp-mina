package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;
import java.util.Date;

public class ElevatorFaultVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3055567695181435654L;
	private Integer id;
	private String elevatorId;//电梯编号
	private Date elevatorFaultTime;//电梯故障时间
	private String elevatorFaultCode;//电梯故障code
	private String elecatorFaultIsuser;//电梯是否困人
	private String elevatorFaultFloor;//电梯当前楼层
	private String elevatorFaultDirection;//电梯方向
	private String elevatorFaultRoom;//电梯门状态
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getElecatorFaultIsuser() {
		return elecatorFaultIsuser;
	}
	public void setElecatorFaultIsuser(String elecatorFaultIsuser) {
		this.elecatorFaultIsuser = elecatorFaultIsuser;
	}
	public String getElevatorId() {
		return elevatorId;
	}
	public void setElevatorId(String elevatorId) {
		this.elevatorId = elevatorId;
	}
	public Date getElevatorFaultTime() {
		return elevatorFaultTime;
	}
	public void setElevatorFaultTime(Date elevatorFaultTime) {
		this.elevatorFaultTime = elevatorFaultTime;
	}
	public String getElevatorFaultCode() {
		return elevatorFaultCode;
	}
	public void setElevatorFaultCode(String elevatorFaultCode) {
		this.elevatorFaultCode = elevatorFaultCode;
	}
	public String getElevatorFaultFloor() {
		return elevatorFaultFloor;
	}
	public void setElevatorFaultFloor(String elevatorFaultFloor) {
		this.elevatorFaultFloor = elevatorFaultFloor;
	}
	public String getElevatorFaultDirection() {
		return elevatorFaultDirection;
	}
	public void setElevatorFaultDirection(String elevatorFaultDirection) {
		this.elevatorFaultDirection = elevatorFaultDirection;
	}
	public String getElevatorFaultRoom() {
		return elevatorFaultRoom;
	}
	public void setElevatorFaultRoom(String elevatorFaultRoom) {
		this.elevatorFaultRoom = elevatorFaultRoom;
	}
	
}
