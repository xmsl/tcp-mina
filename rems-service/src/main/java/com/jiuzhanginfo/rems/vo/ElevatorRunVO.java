package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;
/**
 * 
 * @author xumengsi
 *
 */
public class ElevatorRunVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4467557640695530591L;
	//故障状态码
	private String faultCode;
	//当前层站
	private String station;
	//运行模式  0=运行 1=检修
	private String runStatus;
	//操作模式  0=自动 1=司机
	private String operationMode;
	//运行方向	0/1/2 停/上/下
	private String direction;
	//是否有人
	private String havePpl;
	//门状态	0/1/2/3 开门到位/关门到位/正在开门/正在关门
	private String door;
	//平层状态	0/1/2	平层到位/正在平层/不在平层区
	private String flatFloorStatus;
	//是否锁梯 0：运行  1：锁梯
	private String elevatorLock;
	//安全回路 0：正常  1：断开
	private String safetyCircuit;
	//系统电源  0：正常  1：断电
	private String sysPower;
	//门连锁回路 
	private String doorLock;
	//制动器  0：闭合  1：断开
	private String brake;
	//轿厢温度  
	private String carTemp;
	//轿厢湿度
	private String carHum;
	//是否超载0：正常  1：超载
	private String isOverload;

	public String getFaultCode() {
		return faultCode;
	}

	public void setFaultCode(String faultCode) {
		this.faultCode = faultCode;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(String runStatus) {
		this.runStatus = runStatus;
	}

	public String getOperationMode() {
		return operationMode;
	}

	public void setOperationMode(String operationMode) {
		this.operationMode = operationMode;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getHavePpl() {
		return havePpl;
	}

	public void setHavePpl(String havePpl) {
		this.havePpl = havePpl;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public String getFlatFloorStatus() {
		return flatFloorStatus;
	}

	public void setFlatFloorStatus(String flatFloorStatus) {
		this.flatFloorStatus = flatFloorStatus;
	}

	public String getElevatorLock() {
		return elevatorLock;
	}

	public void setElevatorLock(String elevatorLock) {
		this.elevatorLock = elevatorLock;
	}

	public String getSafetyCircuit() {
		return safetyCircuit;
	}

	public void setSafetyCircuit(String safetyCircuit) {
		this.safetyCircuit = safetyCircuit;
	}

	public String getSysPower() {
		return sysPower;
	}

	public void setSysPower(String sysPower) {
		this.sysPower = sysPower;
	}

	public String getDoorLock() {
		return doorLock;
	}

	public void setDoorLock(String doorLock) {
		this.doorLock = doorLock;
	}

	public String getBrake() {
		return brake;
	}

	public void setBrake(String brake) {
		this.brake = brake;
	}

	public String getCarTemp() {
		return carTemp;
	}

	public void setCarTemp(String carTemp) {
		this.carTemp = carTemp;
	}

	public String getCarHum() {
		return carHum;
	}

	public void setCarHum(String carHum) {
		this.carHum = carHum;
	}

	public String getIsOverload() {
		return isOverload;
	}

	public void setIsOverload(String isOverload) {
		this.isOverload = isOverload;
	}

}
