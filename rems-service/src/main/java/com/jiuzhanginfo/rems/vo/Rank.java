package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;

/**
 * 存储报错通知等级
 * @author ldd
 *
 */
public class Rank implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3232058831952597276L;
	private String rank;

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public Rank(String rank) {
		super();
		this.rank = rank;
	}
	
	
}
