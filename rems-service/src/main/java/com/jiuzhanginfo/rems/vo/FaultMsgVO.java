package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;

public class FaultMsgVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 578920353338634593L;
	private Integer id;
	private String elevatorId;//电梯编号
	private String messageUser;//通知人员
	private String priorityRank;//优先级别
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getElevatorId() {
		return elevatorId;
	}
	public void setElevatorId(String elevatorId) {
		this.elevatorId = elevatorId;
	}
	public String getMessageUser() {
		return messageUser;
	}
	public void setMessageUser(String messageUser) {
		this.messageUser = messageUser;
	}
	public String getPriorityRank() {
		return priorityRank;
	}
	public void setPriorityRank(String priorityRank) {
		this.priorityRank = priorityRank;
	}
	
}
