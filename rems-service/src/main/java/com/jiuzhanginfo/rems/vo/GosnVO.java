package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author xumengsi
 * @param <T>
 *
 */
public class GosnVO<T> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 372070893749475999L;
	
	private List<T> analysisObj;

	public List<T> getAnalysisObj() {
		return analysisObj;
	}

	public void setAnalysisObj(List<T> analysisObj) {
		this.analysisObj = analysisObj;
	}
}
