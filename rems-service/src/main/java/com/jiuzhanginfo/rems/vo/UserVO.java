package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;

/**
 * 
 * @author xumengsi
 *
 */
public class UserVO  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6365366717234248247L;
	
	private Integer id;
	/**
	 * 用户名
	 */
	private String userName;
	/**
	 * 密码
	 */
	private String userPassword;
	/**
	 * app登录的用户名
	 */
	private String userNickname;
	/**
	 * 用户角色
	 */
	private String userRole;
	/**
	 * 用户手机号
	 */
	private String userMoble;
	/**
	 * 用户环信用户名
	 */
	private String userHXId;
	/**
	 * 用户地址
	 */
	private String userAddress;
	/**
	 * 用户邮箱
	 */
	private String userMail;
	/**
	 * 用户性别
	 */
	private String userSex;
	/**
	 * 用户身份证号
	 */
	private String userIdentity;
	/**
	 * 所有公司
	 */
	private String userCompany;
	/**
	 * 客户端Ip
	 */
	private String clientId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserNickname() {
		return userNickname;
	}

	public void setUserNickname(String userNickname) {
		this.userNickname = userNickname;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public String getUserMoble() {
		return userMoble;
	}

	public void setUserMoble(String userMoble) {
		this.userMoble = userMoble;
	}

	public String getUserHXId() {
		return userHXId;
	}

	public void setUserHXId(String userHXId) {
		this.userHXId = userHXId;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserMail() {
		return userMail;
	}

	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}

	public String getUserIdentity() {
		return userIdentity;
	}

	public void setUserIdentity(String userIdentity) {
		this.userIdentity = userIdentity;
	}

	public String getUserCompany() {
		return userCompany;
	}

	public void setUserCompany(String userCompany) {
		this.userCompany = userCompany;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

}
