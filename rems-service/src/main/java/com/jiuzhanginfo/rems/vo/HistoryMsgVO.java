package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;
import java.util.Date;

public class HistoryMsgVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -466107774092611179L;
	private String faultType;//故障类型
	private Date faultTime;//故障时间
	private Date userArriveTime;//用户到达时间
	private Date userFinishTime;//用户完成时间
	private Date userUploadTime;//用户上报时间
	private Date examineTime;//审批时间
	private String attachmentList;//附件列表
	private String elevatorId;//电梯编号
	private Integer elevatorFaultId;//elevator_fault表的ID
	public String getFaultType() {
		return faultType;
	}
	public void setFaultType(String faultType) {
		this.faultType = faultType;
	}
	public Date getFaultTime() {
		return faultTime;
	}
	public void setFaultTime(Date faultTime) {
		this.faultTime = faultTime;
	}
	public Date getUserArriveTime() {
		return userArriveTime;
	}
	public void setUserArriveTime(Date userArriveTime) {
		this.userArriveTime = userArriveTime;
	}
	public Date getUserFinishTime() {
		return userFinishTime;
	}
	public void setUserFinishTime(Date userFinishTime) {
		this.userFinishTime = userFinishTime;
	}
	public Date getUserUploadTime() {
		return userUploadTime;
	}
	public void setUserUploadTime(Date userUploadTime) {
		this.userUploadTime = userUploadTime;
	}
	public Date getExamineTime() {
		return examineTime;
	}
	public void setExamineTime(Date examineTime) {
		this.examineTime = examineTime;
	}
	public String getAttachmentList() {
		return attachmentList;
	}
	public void setAttachmentList(String attachmentList) {
		this.attachmentList = attachmentList;
	}
	public String getElevatorId() {
		return elevatorId;
	}
	public void setElevatorId(String elevatorId) {
		this.elevatorId = elevatorId;
	}
	public Integer getElevatorFaultId() {
		return elevatorFaultId;
	}
	public void setElevatorFaultId(Integer elevatorFaultId) {
		this.elevatorFaultId = elevatorFaultId;
	}
	
}
