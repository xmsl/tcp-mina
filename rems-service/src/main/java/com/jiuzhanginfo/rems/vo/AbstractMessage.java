package com.jiuzhanginfo.rems.vo;

/**
 * <p>File：AbstractMessage.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * @author xumensgi
 * @version 1.0
 */
public class AbstractMessage
{

	/**
	 * 传输的数据
	 */
	private Object data;
	/**
	 * 数据加密
	 */
	private String md5Str;

	public AbstractMessage(){
		
	}
    
	public AbstractMessage(Object data,String md5Str){
		this.data = data;
		this.md5Str = md5Str;
	}
	
	public Object getData()
	{
		return data;
	}

	public void setData(Object data)
	{
		this.data = data;
	}

	public String getMd5Str() {
		return md5Str;
	}

	public void setMd5Str(String md5Str) {
		this.md5Str = md5Str;
	}
	
}
