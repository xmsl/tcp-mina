package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;
import java.util.Date;

public class FaultEarlyVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8529984035709139632L;
	private Integer id;
	private Date createTime;//创建时间
	private String triggerCondition;//出发条件
	private String isAram;//是否报警
	private String messageType;//通知类型
	private String messageInterval;//通知间隔
//	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getTriggerCondition() {
		return triggerCondition;
	}
	public void setTriggerCondition(String triggerCondition) {
		this.triggerCondition = triggerCondition;
	}
	public String getIsAram() {
		return isAram;
	}
	public void setIsAram(String isAram) {
		this.isAram = isAram;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public String getMessageInterval() {
		return messageInterval;
	}
	public void setMessageInterval(String messageInterval) {
		this.messageInterval = messageInterval;
	}
	
}
