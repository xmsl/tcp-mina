package com.jiuzhanginfo.rems.vo;

public class ConstantVO {
	
	/**
	 * 保存客户端的session的主键策略
	 */
	public final static String SESSION_STRATEGY = "SESSION_"; 
	/**
	 * 保存电梯MAP集合
	 */
	public final static String ELEVATOR_MAP_KEY = "ELEVATOR_MAP_";
	/**
	 * 保存电梯Key
	 */
	public final static String ELEVATOR_KEY = "ELEVATOR_";
	
	/**
	 * 认证的Key
	 */
	public final static String ELEVATOR_CLIENT_KEY = "xhyyrems2017" ;
	
	/**
	 * 从 yunba.io 获取应用的 appkey 
	 */
	public final static String  YUNBA_APPKEY = "595c66fa2a9275716fe3ba1f";

}
