package com.jiuzhanginfo.rems.vo;

import java.io.Serializable;

/**
 * 
 * @author xumengsi
 * 
 */
public class ElevatorVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2622877495362542325L;
	// 时间戳
	private String timestamp;
	// 设备类型
	private String deviceType;
	// 协议版本号
	private String protocolVersion;
	// 设别识别码
	private String sn;
	// 实时运行参数
	private ElevatorRunVO data;
	/**
	 * 签名
	 */
	private String sign;

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public ElevatorRunVO getData() {
		return data;
	}

	public void setData(ElevatorRunVO data) {
		this.data = data;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

}
