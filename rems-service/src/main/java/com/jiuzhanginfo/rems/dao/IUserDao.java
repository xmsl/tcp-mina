package com.jiuzhanginfo.rems.dao;

import java.util.List;

import com.jiuzhanginfo.rems.vo.UserVO;

public interface IUserDao {

	List<UserVO> getUserListByRankAndElevatorId(String elevatorId,String rank);
}
