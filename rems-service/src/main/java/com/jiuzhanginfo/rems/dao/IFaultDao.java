package com.jiuzhanginfo.rems.dao;

import com.jiuzhanginfo.rems.vo.FaultEarlyVO;
import com.jiuzhanginfo.rems.vo.FaultMsgVO;

public interface IFaultDao {

	//根据电梯ID查询
	FaultEarlyVO getFaultEarlyByElevatorId(String elevatorId);
	
	//根据电梯ID及级别查询通知人员
	FaultMsgVO getFaultMsgVOByRank(String elevatorId,String rank);
}
