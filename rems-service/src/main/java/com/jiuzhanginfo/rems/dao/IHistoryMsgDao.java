package com.jiuzhanginfo.rems.dao;

import com.jiuzhanginfo.rems.vo.HistoryMsgVO;

public interface IHistoryMsgDao {

	HistoryMsgVO getHistoryMsgVOByElevatorFaultId(int elevatorFaultId);	
}
