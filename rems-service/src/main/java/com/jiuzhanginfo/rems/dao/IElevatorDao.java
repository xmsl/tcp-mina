package com.jiuzhanginfo.rems.dao;

import org.apache.ibatis.annotations.Param;

import com.jiuzhanginfo.rems.vo.ElevatorFaultVO;
import com.jiuzhanginfo.rems.vo.ElevatorVO;

public interface IElevatorDao {

	void elevatorUploadTrouble(@Param("elevatorVO") ElevatorVO elevatorVO);
	
	/**
	 * 根据电梯编号获取最新的故障记录
	 * @param sn
	 * @return
	 */
	ElevatorFaultVO getElevatorVOlatest(@Param("sn") String sn);

	ElevatorFaultVO getElevatorVOBySnFaultCode(@Param("sn") String sn,@Param("faultCode") String faultCode);
}
