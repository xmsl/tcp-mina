package com.jiuzhanginfo.rems.mina.code;



import java.nio.charset.Charset;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jiuzhanginfo.rems.util.ByteUtil;
import com.jiuzhanginfo.rems.vo.ElevatorVO;
/**
 * 
 * @author xumengsi
 *
 */
@SuppressWarnings("unused")
public class ByteArrayDecoder extends CumulativeProtocolDecoder {

	private final Charset charset;
	//包头长度
	private static int HEARDLENGTH = 4;
	
	public ByteArrayDecoder(Charset charset) {
		this.charset = charset;
	}
	/**
	 * 解码器
	 */
	@Override
	protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
		  if(in.remaining()<4){
			  return false;
		  }else{
			// 标记开始位置，如果一条消息没传输完成则返回到这个位置  
	         in.mark(); 
	         byte[] bytes = new byte[4];  
	         in.get(bytes); // 读取4字节的Header 
	         int bodyLength = LittleEndian.getLittleEndianInt(bytes); // 按小字节序转int 
	      // 如果body没有接收完整，直接返回false
	         if(in.remaining() < bodyLength) {  
	                in.reset(); // IoBuffer position回到原来标记的地方  
	                return false;  
	            }else {  
	                byte[] bodyBytes = new byte[bodyLength];  
	                in.get(bodyBytes);  
	                String res = new String(bodyBytes);
	                JSONObject json = JSONObject.parseObject(res);
	                System.out.println("服务端接收到的信息：--》"  + json);
	                ElevatorVO vo =  JSON.parseObject(res, ElevatorVO.class);
	                out.write(vo); // 解析出一条消息  
	                return true;
	            }  
		  }
	};
};