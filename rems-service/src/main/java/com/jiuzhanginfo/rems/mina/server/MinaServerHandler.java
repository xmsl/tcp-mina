package com.jiuzhanginfo.rems.mina.server;



import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import com.jiuzhanginfo.rems.services.IElevatorService;
import com.jiuzhanginfo.rems.socket.IOCallback;
import com.jiuzhanginfo.rems.util.MD5Tools;
import com.jiuzhanginfo.rems.util.SessionMapUtil;
import com.jiuzhanginfo.rems.vo.ConstantVO;
import com.jiuzhanginfo.rems.vo.ElevatorVO;

public class MinaServerHandler extends IoHandlerAdapter {
	
	@Resource
	private IElevatorService elevatorService;
	@Resource
	private IOCallback ioCallback;
	
	private SessionMapUtil sessionMapUtil = SessionMapUtil.newInstance();
	
	private final int IDLE = 30;
	
	private final Logger LOG = Logger.getLogger(MinaServerHandler.class);
	
	@Override
	public void exceptionCaught(IoSession session, Throwable cause) throws Exception {
		session.closeOnFlush();
		LOG.warn("session occured exception, so close it." + cause.getMessage());
		LOG.warn(cause.toString());
	}

	
	@Override
	@SuppressWarnings("deprecation")
	public void messageReceived(IoSession session, Object message) throws Exception {
		ElevatorVO elevatorVO = (ElevatorVO)message;
		//得到采集板的token
		String sign = elevatorVO.getSign();
		LOG.info("采集板签名:"+sign);
		String sn = elevatorVO.getSn();
		String timestamp = elevatorVO.getTimestamp();
		String md5Key = sn + timestamp + ConstantVO.ELEVATOR_CLIENT_KEY;
		//认证
		if(!sign.equals(MD5Tools.MD5(md5Key))){
			LOG.info("认证失败");
			session.close();
		}else{
			//通过订阅号发布
			ioCallback.socketLienter(elevatorVO);
			String sessionKey = ConstantVO.SESSION_STRATEGY+sn ; 
			if(sessionMapUtil.getSession(sessionKey)==null){
				sessionMapUtil.addSession(sessionKey, session);
			};
			elevatorService.elevatorUploadTrouble(sn, elevatorVO);
		};
	};

	@Override
	public void messageSent(IoSession session, Object message) throws Exception {
		LOG.warn("messageSent:" + message);
	}

	@Override
	public void sessionCreated(IoSession session) throws Exception {
		LOG.warn("remote client [" + session.getRemoteAddress().toString() + "] connected.");
	}

	@Override
	public void sessionClosed(IoSession session) throws Exception {
		LOG.warn("sessionClosed.");
		session.closeOnFlush();
	}

	@Override
	public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
		LOG.warn("session idle");
		System.out.println(System.currentTimeMillis());
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		LOG.warn("sessionOpened.");
		session.getConfig().setIdleTime(IdleStatus.BOTH_IDLE, IDLE);
	}
	
	
	
	
}
