package com.jiuzhanginfo.rems.mina.code;

import java.nio.charset.Charset;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoderAdapter;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jiuzhanginfo.rems.vo.AbstractMessage;

@SuppressWarnings("unused")
public class ByteArrayEncoder extends ProtocolEncoderAdapter {

	private final Charset charset;
	public ByteArrayEncoder(Charset charset) {
		this.charset = charset;
	}
	
	private static final Logger logger = LoggerFactory.getLogger(ByteArrayEncoder.class);
	
	@Override
	public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
		AbstractMessage msg = (AbstractMessage)message; 
		Object obj = msg.getData();
		String md5Str =  msg.getMd5Str();
		String dataMsg = (String) obj;
		String msgStr = dataMsg + md5Str ;
		byte[] bytes = msgStr.getBytes("UTF-8");
		int length = bytes.length;
		byte[] header = LittleEndian.toLittleEndian(length); // 按小字节序转成字节数组 
		IoBuffer buffer = IoBuffer.allocate(length + 4);
		buffer.put(header); // header  
        buffer.put(bytes); // body  
        buffer.flip();  
        out.write(buffer); 
	}

}
