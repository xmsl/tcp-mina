package com.jiuzhanginfo.rems.services.impl;

import java.net.MalformedURLException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.jiuzhanginfo.rems.socket.IOAcknowledge;
import com.jiuzhanginfo.rems.socket.IOCallback;
import com.jiuzhanginfo.rems.socket.SocketIO;
import com.jiuzhanginfo.rems.socket.SocketIOException;
import com.jiuzhanginfo.rems.vo.ElevatorVO;

@Service("socketIOService")
public class SocketIOService implements IOCallback{
	
	
	Logger logger = LoggerFactory.getLogger(SocketIOService.class);
	
	   private SocketIO socket;
	   /* 从 yunba.io 获取应用的 appkey */
	   private static String APPKEY = "595c66fa2a9275716fe3ba1f";
	   
	   @Override
	   public void socketLienter(ElevatorVO elevatorVO) throws Exception {
		   if(socket!=null){
			   if(!socket.isConnected()){
				   socket = new SocketIO();
				   socket.connect("http://sock.yunba.io:3000/", this); 
			   }
		   }else{
				   socket = new SocketIO();
				   socket.connect("http://sock.yunba.io:3000/", this);
		   };
		   logger.info("#########################发送消息########################################");
	       socket.emit("publish", new JSONObject("{'topic': 'rems', 'msg':'" + com.alibaba.fastjson.JSONObject.toJSON(elevatorVO).toString() + "', 'qos': 1}"));
		};

	@Override
	public void onDisconnect() {
		   try {
			socket = new SocketIO();
			socket.connect("http://sock.yunba.io:3000/", this);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onConnect() {
		logger.info("连接成功");
	}

	@Override
	public void onMessage(String data, IOAcknowledge ack) {
		
	}

	@Override
	public void onMessage(JSONObject json, IOAcknowledge ack) {
		
	}

	@Override
	public void on(String event, IOAcknowledge ack, Object... args) {
		System.out.println("响应事件类型：'" + event + "'");
        try {
            if (event.equals("socketconnectack")) {//连接服务器成功后的回调
                onSocketConnectAck();
            } else if (event.equals("connack")) { //服务器确认身份的回调    
                onConnAck((JSONObject)args[0]);
            } else if (event.equals("puback")) { //推送成功后的回调
                onPubAck((JSONObject)args[0]);
            } else if (event.equals("message")){ //获取到新消息的事件
                onGetMessage((JSONObject)args[0]);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
	
		
	}

	@Override
	public void onError(SocketIOException socketIOException) {
		
	}
	
	public void onSocketConnectAck() throws Exception {
		logger.info("MQTT服务器连接成功");
		logger.info("请求确认身份");
		socket.emit("connect_v2", new JSONObject("{'appkey': '" + APPKEY + "', 'customid': '" + "xms" + "'}"));
		
	}
	
   public void onConnAck(JSONObject json) throws Exception { 
	    logger.info("服务器确认身份：" + json.get("success"));
	    logger.info("绑定别名：xms");
	    socket.emit("set_alias", new JSONObject("{'alias': 'xms'}"));
	    logger.info("增加订阅频道:rems");
	    socket.emit("subscribe", new JSONObject("{'topic': 'rems'}"));
   }    

	public void onPubAck(JSONObject json) throws Exception {
		logger.info("发送状态：" + json.get("success") + " 发送id：" + json.get("messageId"));
	};
	
	public void onGetMessage(JSONObject json) throws Exception{
		logger.info("收到新的消息："+ json.get("msg") );
	}

}
