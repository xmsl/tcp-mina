package com.jiuzhanginfo.rems.services;

import com.jiuzhanginfo.rems.vo.ElevatorVO;

/**
 * 
 * @author xumengsi
 *
 */
public interface IElevatorService {
	
	/**
	 * 电梯数据上报
	 * @param token
	 * @param elevatorVO
	 */
	ElevatorVO elevatorUploadTrouble(String sn,ElevatorVO elevatorVO)throws Exception;
	
}
