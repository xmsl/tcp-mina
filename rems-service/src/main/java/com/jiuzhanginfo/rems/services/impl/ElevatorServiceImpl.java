package com.jiuzhanginfo.rems.services.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.jiuzhanginfo.rems.dao.IElevatorDao;
import com.jiuzhanginfo.rems.dao.IFaultDao;
import com.jiuzhanginfo.rems.dao.IHistoryMsgDao;
import com.jiuzhanginfo.rems.dao.IUserDao;
import com.jiuzhanginfo.rems.redis.annotation.ParameterValueKeyProvider;
import com.jiuzhanginfo.rems.redis.annotation.ReadCacheType;
import com.jiuzhanginfo.rems.redis.annotation.UpdateThroughAssignCache;
import com.jiuzhanginfo.rems.redis.cache.ICacheService;
import com.jiuzhanginfo.rems.services.IElevatorService;
import com.jiuzhanginfo.rems.vo.ElevatorFaultVO;
import com.jiuzhanginfo.rems.vo.ElevatorVO;
import com.jiuzhanginfo.rems.vo.FaultEarlyVO;
import com.jiuzhanginfo.rems.vo.FaultMsgVO;
import com.jiuzhanginfo.rems.vo.HistoryMsgVO;
import com.jiuzhanginfo.rems.vo.Rank;
import com.jiuzhanginfo.rems.vo.UserVO;

/**
 * 
 * @author xumengsi
 *
 */
@Service("elevatorServiceImpl")
public class ElevatorServiceImpl implements IElevatorService{
	
	private final Logger LOG = Logger.getLogger(ElevatorServiceImpl.class);
	
	@Resource
	private IElevatorDao elevatorDao;
	@Resource
	private ICacheService cacheService;
	@Resource
	private IHistoryMsgDao historyMsgDao;
	@Resource
	private IFaultDao faultDao;
	@Resource
	private IUserDao userDao;

	/**
	 * 存缓存中
	 */
	@Override
	@UpdateThroughAssignCache(namespace="ELEVATOR",cacheType = ReadCacheType.String,valueclass =ElevatorVO.class,expireTime=0)
	public ElevatorVO elevatorUploadTrouble(@ParameterValueKeyProvider String sn, ElevatorVO elevatorVO) throws Exception {
		LOG.info("开始数据处理");
		if(!elevatorVO.getData().getFaultCode().equals("0")){
			
			ElevatorFaultVO elevatorFaultVO=elevatorDao.getElevatorVOBySnFaultCode(sn, elevatorVO.getData().getFaultCode());
			FaultEarlyVO faultEarlyVO=faultDao.getFaultEarlyByElevatorId(sn);//获取电梯报错通知信息
			if(elevatorFaultVO==null){//1：故障首次报错--添加到数据库
				elevatorVO.setTimestamp(stampToDate(elevatorVO.getTimestamp()));
				elevatorDao.elevatorUploadTrouble(elevatorVO);
				
				//通知维保人员
				List<UserVO> userList=userDao.getUserListByRankAndElevatorId(sn,"1");
				elevatorFaultVO=elevatorDao.getElevatorVOlatest(sn);//获取刚刚入库的故障记录
				Rank rank=new Rank("1");//构建等级
				cacheService.set("ElevatorFault_"+elevatorFaultVO.getId(), rank, 0);//将通知等级存入缓存,注意key存储格式				
				
				if(faultEarlyVO.getMessageType()=="1"){//依据通知类型选择短信通知或邮件通知
					
				}else{
					
				}
				
				
				
			}else{//:1：非首次报错，已入库--查看是否已处理
				HistoryMsgVO historyMsgVO=historyMsgDao.getHistoryMsgVOByElevatorFaultId(elevatorFaultVO.getId());
				if(historyMsgVO==null){//该故障未处理--通知维保人员
					Rank rank=((Rank) cacheService.get("ElevatorFault_"+elevatorFaultVO.getId(), Rank.class));
					int rankNum=Integer.parseInt(rank.getRank())+1;
					rank.setRank(rankNum+"");
					List<UserVO> userList=userDao.getUserListByRankAndElevatorId(sn,rank.getRank());//获取应通知的维保人员
					
					if(faultEarlyVO.getMessageType()=="1"){//依据通知类型选择短信通知或邮件通知
						
					}else{
						
					}
				}//故障已处理--无
			}
			
			
			
		}
		return elevatorVO;
	};
	
	
	 /* 
     * 将时间戳转换为时间
     */
    public static String stampToDate(String s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    };

}
