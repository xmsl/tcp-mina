package com.jiuzhanginfo.rems.services;

import com.jiuzhanginfo.rems.vo.ElevatorFaultVO;

public interface IElevatorFaultService {

	ElevatorFaultVO getElevatorFaultVOByElevatorFaultId(String elevatorFaultId);
}
