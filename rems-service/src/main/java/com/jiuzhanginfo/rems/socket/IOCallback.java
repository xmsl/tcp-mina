/*
 * socket.io-java-client IOCallback.java
 *
 * Copyright (c) 2012, ${author}
 * socket.io-java-client is a implementation of the socket.io protocol in Java.
 * 
 * See LICENSE file for more information
 */
package com.jiuzhanginfo.rems.socket;

import org.json.JSONObject;

import com.jiuzhanginfo.rems.vo.ElevatorVO;

/**
 * The Interface IOCallback. A callback interface to SocketIO
 */
public interface IOCallback {
	
	
	public void socketLienter(ElevatorVO elevatorVO) throws Exception;
	
	/**
	 * 连接断开时调用
	 */
	void onDisconnect();
	
	/**
	 * 连接时调用
	 */
	void onConnect();
	
	/**
	 * 发送字符串数据时调用
	 *
	 * @param data the data.
	 * @param ack an {@link IOAcknowledge} instance, may be <code>null</code> if there's none
	 */
	void onMessage(String data, IOAcknowledge ack);
	
	/**
	 * 服务器发送JSON数据时调用
	 * @param json JSON object sent by server.
	 * @param ack an {@link IOAcknowledge} instance, may be <code>null</code> if there's none
	 */
	void onMessage(JSONObject json, IOAcknowledge ack);
	
	/**
	 * 服务器发出事件时调用
	 * @param event Name of the event
	 * @param ack an {@link IOAcknowledge} instance, may be <code>null</code> if there's none
	 * @param args Arguments of the event
	 */
	void on(String event, IOAcknowledge ack, Object... args);
	
	/**
	 * 服务器连接失败时调用
	 *
	 * @param socketIOException the last exception describing the error
	 */
	void onError(SocketIOException socketIOException);
}
