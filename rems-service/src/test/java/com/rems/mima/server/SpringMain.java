package com.rems.mima.server;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * <p>File：SpringMain.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * @author xumengsi
 * @version 1.0
 */
public class SpringMain
{

	public static void main(String[] args) throws Exception
	{
		
		getApplicationContext();
		System.out.println("Listening ...");
	}

	public static ConfigurableApplicationContext getApplicationContext()
	{
		return new ClassPathXmlApplicationContext("classpath:spring.xml");
	}
	
}
